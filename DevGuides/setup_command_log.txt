# Install WittyPi2 Software
Following guide: WittyPi2_UserManual.pdf

wget http://www.uugear.com/repo/WittyPi2/installWittyPi.sh
sudo sh installWittyPi.sh

----

# Camera Suite
Download Link: http://www.camerasuite.org/dl/camerasuitepi.tar.gz
Email: angeal.gabereau@gmail.com
Serial number: 7980026B9E8D0202

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install openssl sqlite libts-0.0 libinput5 libgles2-mesa libstdc++$
libc6 libegl1-mesa libegl1-mesa-drivers libexpat1 libz1 libpng12-0 libevdev2 li$
libxdmcp6 libxau6 libfreetype6 libfontconfig1 libmtdev1 libudev1 libxkbcommon0 $
libx11-6 libx11-xcb1 libxext6 libts-0.0-0 libxcb1 libdbus-1.3
Had to run those twice, and restart.

tar xfv camerasuitepi.tar.gz
cd camerasuite
./camerasuite.sh -platform xcb

pip install --upgrade --user awscli

^DSD(H)MIh7$

----

Dataplicity: Control Raspberry Pi from anywhere.
https://www.dataplicity.com/
curl https://www.dataplicity.com/jpw82l5n.py | sudo python

----

Installed Node.

----

GoPro Wifi Connection
name: shutterisland
password: c10db3rry

----

HUAWEI LTE E8372
name: admin
password: C10u63rry

----

GoPro MAC Address: D4:D9:19:9A:00:5A
serail number
C312112
5863006

----

CamDo Wifi Connection
name: CamDoBlink_F8F005F4EDC1
password: shutterisland
ip address of web interface: http://192.168.1.1

----

Wake On LAN
sudo apt-get install wakeonlan

wakeonlan -i 10.5.5.9 D4:D9:19:9A:00:5A

----

Raspberry Pi Wifi Setup
Can see what networks go pro will connect to here:
sudo nano /etc/network/interfaces
under 
allow-hotplug wlan0
which direct to:
sudo nano /etc/wpa_supplicant/wpa_supplicant.conf

----

When connected via usb, can navigate dir here: gphoto2://[usb:001,012]/DCIM/100GOPRO
